import pandas as pd
import matplotlib.pyplot as plt


class Analysis:
    def __init__(self, data) -> None:
        self.data = data

    def read_data(self) -> pd.DataFrame:
        return pd.read_csv(self.data, chunksize=1000)

    def get_data(self) -> pd.DataFrame:
        return self.read_data()

    def get_data_by_chunk(self, chunksize: int) -> pd.DataFrame:
        return self.read_data(chunksize)

    def concat_data(self, data: pd.DataFrame) -> pd.DataFrame:
        return pd.concat(data, ignore_index=True)

    def optimize_memory(self, data: pd.DataFrame) -> pd.DataFrame:
        return data.astype({
            'Timestamp(ms)': 'int32',
            'Current(uA)': 'int16',
        })

    # convert timestamp column to datetime object
    def convert_timestamp(self, data: pd.DataFrame) -> pd.DataFrame:
        return pd.to_datetime(data['Timestamp(ms)'], unit='ms')

    # convert datetime object to seconds and minutes
    def convert_datetime(self, data: pd.DataFrame) -> pd.DataFrame:
        return data['Timestamp(ms)'].dt.second + data['Timestamp(ms)'].dt.minute * 60

    # convert current from uA to mA
    def convert_current(self, data: pd.DataFrame) -> pd.DataFrame:
        return data['Current(uA)'] / 1000

    def run(self) -> pd.DataFrame:
        data = self.get_data()
        data = self.concat_data(data)
        data = self.optimize_memory(data)
        data['Timestamp(ms)'] = self.convert_timestamp(data)
        data['Timestamp(ms)'] = self.convert_datetime(data)
        data['Current(uA)'] = self.convert_current(data)

        return data

    def run_by_chunk(self, chunksize: int) -> pd.DataFrame:
        data = self.get_data_by_chunk(chunksize)
        data = self.concat_data(data)
        data = self.optimize_memory(data)
        data['Timestamp(ms)'] = self.convert_timestamp(data)
        data['Timestamp(ms)'] = self.convert_datetime(data)
        data['Current(uA)'] = self.convert_current(data)

        return data

    def current_vs_time(self, data: pd.DataFrame, type: str, device: str, slice: list = []) -> None:
        if slice:
            data = data.iloc[slice[0]:slice[-1]]

        match type:
            case 'mean':
                data.groupby('Timestamp(ms)')['Current(uA)'].mean().plot()
                plt.title('Mean: ' + str(round(data['Current(uA)'].mean(), 2)) + ' mA', fontsize=10)

            case 'max':
                data.groupby('Timestamp(ms)')['Current(uA)'].max().plot()
                plt.title('Max: ' + str(round(data['Current(uA)'].max(), 2)) + ' mA', fontsize=10)

            case 'min':
                data.groupby('Timestamp(ms)')['Current(uA)'].min().plot()
                plt.title(
                    'Min: ' + str(round(data['Current(uA)'].min(), 2)) + ' mA', fontsize=10)

            case 'all':
                data.groupby('Timestamp(ms)')['Current(uA)'].mean().plot(label='Mean: ' + str(round(data['Current(uA)'].mean(), 2)) + ' mA')
                data.groupby('Timestamp(ms)')['Current(uA)'].max().plot(label='Max: ' + str(round(data['Current(uA)'].max(), 2)) + ' mA')
                data.groupby('Timestamp(ms)')['Current(uA)'].min().plot(label='Min: ' + str(round(data['Current(uA)'].min(), 2)) + ' mA')
                plt.legend()

            case 'individual':
                _, axs = plt.subplots(1, 3, figsize=(15, 5))

                axs[0].plot(data.groupby('Timestamp(ms)')['Current(uA)'].mean())
                axs[0].set_xlabel('Time (s)')
                axs[0].set_ylabel('Current (mA)')
                axs[0].set_title('Mean: ' + str(round(data['Current(uA)'].mean(), 2)) + ' mA', fontsize=10)

                axs[1].plot(data.groupby('Timestamp(ms)')['Current(uA)'].max())
                axs[1].set_xlabel('Time (s)')
                axs[1].set_ylabel('Current (mA)')
                axs[1].set_title('Max: ' + str(round(data['Current(uA)'].max(), 2)) + ' mA', fontsize=10)

                axs[2].plot(data.groupby('Timestamp(ms)')['Current(uA)'].min())
                axs[2].set_xlabel('Time (s)')
                axs[2].set_ylabel('Current (mA)')
                axs[2].set_title('Min: ' + str(round(data['Current(uA)'].min(), 2)) + ' mA', fontsize=10)

            case '_':
                plt.plot(data['Timestamp(ms)'], data['Current(uA)'])
                plt.title('Mean: ' + str(round(data['Current(uA)'].mean(), 2)) + ' mA, Max: ' + str(round(data['Current(uA)'].max(), 2)) + ' mA, Min: ' + str(round(data['Current(uA)'].min(), 2)) + ' mA', fontsize=10)

        plt.xlabel('Time (s)')
        plt.ylabel('Current (mA)')
        plt.suptitle('Current vs Time: ' + device, fontsize=16)
        plt.show()

        return None

    def compare_current_vs_time(self, data: list, type: str, device: list, slice: list = []) -> None:
        if slice:
            for i in range(len(data)):
                data[i] = data[i].iloc[slice[0]:slice[-1]]

        match type:
            case 'mean':
                for i in range(len(data)):
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].mean().plot(label=device[i] + ': ' + str(round(data[i]['Current(uA)'].mean(), 2)) + ' mA')

                plt.title('Mean', fontsize=10)
            case 'max':
                for i in range(len(data)):
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].max().plot(label=device[i] + ': ' + str(round(data[i]['Current(uA)'].max(), 2)) + ' mA')

                plt.title('Maximum', fontsize=10)

            case 'min':
                for i in range(len(data)):
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].min().plot(label=device[i] + ': ' + str(round(data[i]['Current(uA)'].min(), 2)) + ' mA')

                plt.title('Minimum', fontsize=10)

            case 'individual':
                _, axs = plt.subplots(1, 3, figsize=(15, 5))

                [axs[0].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].mean(), label=device[i] + ': ' + str(round(data[i]['Current(uA)'].mean(), 2)) + ' mA') for i in range(len(data))]
                [axs[1].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].max(), label=device[i] + ': ' + str(round(data[i]['Current(uA)'].max(), 2)) + ' mA') for i in range(len(data))]
                [axs[2].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].min(), label=device[i] + ': ' + str(round(data[i]['Current(uA)'].min(), 2)) + ' mA') for i in range(len(data))]

                [axs[i].set_xlabel('Time (s)') for i in range(len(axs))]
                [axs[i].set_ylabel('Current (mA)') for i in range(len(axs))]
                [axs[i].legend() for i in range(len(axs))]

                axs[0].set_title('Mean', fontsize=10)
                axs[1].set_title('Maximum', fontsize=10)
                axs[2].set_title('Minimum', fontsize=10)

            case _:
                for i in range(len(data)):
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].mean().plot(label=device[i] + ' Mean: ' + str(round(data[i]['Current(uA)'].mean(), 2)) + ' mA')
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].max().plot(label=device[i] + ' Max: ' + str(round(data[i]['Current(uA)'].max(), 2)) + ' mA')
                    data[i].groupby('Timestamp(ms)')['Current(uA)'].min().plot(label=device[i] + ' Min: ' + str(round(data[i]['Current(uA)'].min(), 2)) + ' mA')

        plt.xlabel('Time (s)')
        plt.ylabel('Current (mA)')
        plt.legend()
        plt.suptitle('Current vs Time: ' + ', '.join(device), fontsize=16)
        plt.show()

        return None

    def overview_current_vs_time(self, data: list, type: str, device: list, slice: list = []) -> None:
        if slice:
            for i in range(len(data)):
                data[i] = data[i].iloc[slice[0]:slice[-1]]

        _, axs = plt.subplots(2, 4, figsize=(15, 5))

        match type:
            case 'mean':
                for i in range(len(data)):
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].mean())
                    axs[i // 4, i % 4].set_title(device[i] + ': ' + str(round(data[i]['Current(uA)'].mean(), 2)) + ' mA', fontsize=10)
                    axs[i // 4, i % 4].set_xlabel('Time (s)')
                    axs[i // 4, i % 4].set_ylabel('Current (mA)')

            case 'max':
                for i in range(len(data)):
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].max())
                    axs[i // 4, i % 4].set_title(device[i] + ': ' + str(round(data[i]['Current(uA)'].max(), 2)) + ' mA', fontsize=10)
                    axs[i // 4, i % 4].set_xlabel('Time (s)')
                    axs[i // 4, i % 4].set_ylabel('Current (mA)')

            case 'min':
                for i in range(len(data)):
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].min())
                    axs[i // 4, i % 4].set_title(device[i] + ': ' + str(round(data[i]['Current(uA)'].min(), 2)) + ' mA', fontsize=10)
                    axs[i // 4, i % 4].set_xlabel('Time (s)')
                    axs[i // 4, i % 4].set_ylabel('Current (mA)')

            case '_':
                for i in range(len(data)):
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].mean(), label=device[i] + ' Mean: ' + str(round(data[i]['Current(uA)'].mean(), 2)) + ' mA')
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].max(), label=device[i] + ' Max: ' + str(round(data[i]['Current(uA)'].max(), 2)) + ' mA')
                    axs[i // 4, i % 4].plot(data[i].groupby('Timestamp(ms)')['Current(uA)'].min(), label=device[i] + ' Min: ' + str(round(data[i]['Current(uA)'].min(), 2)) + ' mA')
                    axs[i // 4, i % 4].set_title(device[i], fontsize=10)
                    axs[i // 4, i % 4].set_xlabel('Time (s)')
                    axs[i // 4, i % 4].set_ylabel('Current (mA)')
                    axs[i // 4, i % 4].legend()

        plt.suptitle('Current vs Time: ' + ', '.join(device), fontsize=16)
        plt.tight_layout()
        plt.show()

        return None

    def compare_overview_current_vs_time(self, data: list, type: str, device: list, mode: list, slice: list = []) -> None:
        # data = [[df1, df2], [df1, df2]]
        if slice:
            for i in range(len(data)):
                for j in range(len(data[i])):
                    data[i][j] = data[i][j].iloc[slice[0]:slice[-1]]

        _, axs = plt.subplots(2, 4, figsize=(15, 5))

        match type:
            case 'mean':
                for i in range(len(data)):
                    for j in range(len(data[i])):
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].mean(), label=mode[j] + ': ' + str(round(data[i][j]['Current(uA)'].mean(), 2)) + ' mA')
                        axs[i // 4, i % 4].set_title(device[0][i], fontsize=10)
                        axs[i // 4, i % 4].set_xlabel('Time (s)')
                        axs[i // 4, i % 4].set_ylabel('Current (mA)')
                        axs[i // 4, i % 4].legend()

                plt.suptitle('Current vs Time: (Mean) ' + ', '.join(mode), fontsize=16)

            case 'max':
                for i in range(len(data)):
                    for j in range(len(data[i])):
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].max(), label=mode[j] + ': ' + str(round(data[i][j]['Current(uA)'].max(), 2)) + ' mA')
                        axs[i // 4, i % 4].set_title(device[0][i], fontsize=10)
                        axs[i // 4, i % 4].set_xlabel('Time (s)')
                        axs[i // 4, i % 4].set_ylabel('Current (mA)')
                        axs[i // 4, i % 4].legend()

                plt.suptitle('Current vs Time: (Max) ' + ', '.join(mode), fontsize=16)

            case 'min':
                for i in range(len(data)):
                    for j in range(len(data[i])):
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].min(), label=mode[j] + ': ' + str(round(data[i][j]['Current(uA)'].min(), 2)) + ' mA')
                        axs[i // 4, i % 4].set_title(device[0][i], fontsize=10)
                        axs[i // 4, i % 4].set_xlabel('Time (s)')
                        axs[i // 4, i % 4].set_ylabel('Current (mA)')
                        axs[i // 4, i % 4].legend()

                plt.suptitle('Current vs Time: (Min) ' + ', '.join(mode), fontsize=16)

            case '_':
                for i in range(len(data)):
                    for j in range(len(data[i])):
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].mean(), label=mode[j] + ' Mean: ' + str(round(data[i][j]['Current(uA)'].mean(), 2)) + ' mA')
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].max(), label=mode[j] + ' Max: ' + str(round(data[i][j]['Current(uA)'].max(), 2)) + ' mA')
                        axs[i // 4, i % 4].plot(data[i][j].groupby('Timestamp(ms)')['Current(uA)'].min(), label=mode[j] + ' Min: ' + str(round(data[i][j]['Current(uA)'].min(), 2)) + ' mA')
                        axs[i // 4, i % 4].set_title(device[0][i], fontsize=10)
                        axs[i // 4, i % 4].set_xlabel('Time (s)')
                        axs[i // 4, i % 4].set_ylabel('Current (mA)')
                        axs[i // 4, i % 4].legend()

                plt.suptitle('Current vs Time: (*) ' + ', '.join(mode), fontsize=16)

        plt.tight_layout()
        plt.show()

# instantiate Analysis class
# br1 = Analysis(
#     'analysis/data/maximum/lab/ping/br-1--1/ppk-20230228T210607.csv')
# df1 = br1.run()
# br1.current_vs_time(df1, 'individual', 'Border Router 1')
# br1.mean_max_min_current_vs_time(df1, 'Border Router 1')

# br2 = Analysis(
#     'analysis/data/maximum/lab/ping/br-2--2/ppk-20230228T210657.csv')
# df2 = br2.run()
# br2.mean_max_min_current_vs_time(df, 'Border Router 1')

# br2.compare_mean_current_vs_time([df1, df2], ['BR1', 'BR2'])
# br2.compare_current_vs_time([df1, df2], 'individual', ['BR1', 'BR2'], [0, 6000600])

# br3 = Analysis(
#     'analysis/data/maximum/lab/ping/ed-1--6/ppk-20230228T210730.csv')
# df3 = br3.run()
# # br3.compare_current_vs_time([df1, df2, df3], 'individual', ['BR1', 'BR2', 'BR3'])

# br4 = Analysis(
#     'analysis/data/maximum/lab/ping/ed-1--6/ppk-20230228T210730.csv')
# df4 = br4.run()

# br5 = Analysis(
#     'analysis/data/maximum/lab/ping/ed-1--6/ppk-20230228T210730.csv')
# df5 = br5.run()

# br2.overview_current_vs_time([df1, df2, df3, df4, df5], 'max', ['BR1', 'BR2', 'BR3', 'BR4', 'BR5'])
# br2.overview_current_vs_time([df1, df2], 'mean', ['BR1', 'BR2'])
# br2.compare_overview_current_vs_time([[df1, df2], [df1, df2], [df2, df1], [df2, df1], [df2, df1]], 'mean', [['BR1', 'BR2', 'BR3', 'BR4', 'BR5'], ['BR1', 'BR2', 'BR3', 'BR4', 'BR5']], ['Ping', 'No Sensor', 'Sensor', 'UDP', 'TCP'])
# br2.compare_overview_current_vs_time([[df1, df2], [df1, df2]], '_', [['BR1', 'BR2'], ['BR1', 'BR2']], ['Ping', 'No Sensor'])

# br2.compare_current_vs_time([df1, df2], 'mean', [
#                                          'Border Router 1', 'Border Router 2'], [0, 6000600])
