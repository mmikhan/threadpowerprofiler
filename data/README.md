# Dataset Description

This dataset provides information on modes of transportation, their location, type, and length. Two modes are available: Maximum and Optimized. The dataset is provided in raw `.ppk` format, allowing for easy export through [nRF Power Profiler Kit II SDK](https://www.nordicsemi.com/Products/Development-hardware/Power-Profiler-Kit-2/Download#infotabs) and manipulation in various data analysis tools. The dataset includes distance measurements from two locations: Home and Lab.

In the Home location, the devices are positioned 5 meters apart, comprising 2 border routers, 3 routers (1 acting as the leader), and 3 end devices.

The Lab location provides distance measurements in centimeters, which have been converted to meters for consistency. The distance measurements for this location are as follows:

* Border router device 1 to border router 2 device 2: 0.8 meters
* Border router device 1 to router 2 device 3: 1.65 meters
* Border router device 1 to router 1 device 4: 1.45 meters
* Border router device 1 to router 3 device 5: 2.80 meters
* Border router device 1 to end device 1 device 6: 2.95 meters
* Border router device 1 to end device 2 device 7: 2.65 meters
* Border router device 1 to end device 3 device 8: 2.50 meters

## Mode

There are two modes available in the dataset: Maximum and Optimized. The Maximum mode represents the maximum possible values of the parameters, while the Optimized mode represents the optimized values.

* In maximum mode, the `txpower` is set to 8 dBm.
* In optimized mode, the `txpower` is set followed by the Monte Carlo output and Genetic Algorithm's best fitness values.

## Location

The dataset contains information about two locations: Lab and Home. Each mode is available in both locations. The differences between two locations are the distance between each nodes.

## Type

The dataset has three types of sensors: No sensor, Ping, and UWB. Each mode is available in No sensor and Ping type, while the UWB type is optional.

## Length

The dataset contains data of two lengths: 60 seconds and 300 seconds. Each mode is available in both lengths. The data was taken 100000 sampling per second as such, it would have been very daunting task for computers to load the dataset for longer time period.

## Data Format

The data is available in the following table format:

<table>
    <thead>
        <tr>
            <th>Mode</th>
            <th>Location</th>
            <th>Type</th>
            <th>Length</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Maximum</td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td>Lab</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>Home</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <tbody>
                            <tr>
                                <td>No sensor</td>
                            </tr>
                            <tr>
                                <td>Ping</td>
                            </tr>
                            <tr>
                                <td>UWB</td>
                            </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                            <tr>
                                <td>No sensor</td>
                            </tr>
                            <tr>
                                <td>Ping</td>
                            </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td>60 seconds</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>300 seconds</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>Optimized</td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td>Lab</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>Home</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <tbody>
                            <tr>
                                <td>No sensor</td>
                            </tr>
                            <tr>
                                <td>Ping</td>
                            </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                            <tr>
                                <td>No sensor</td>
                            </tr>
                            <tr>
                                <td>Ping</td>
                            </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td>60 seconds</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>300 seconds</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
